function F_out = func_F(func_g_u,H)
% 
% For v = V \nu, with V = - F(u,H)
% where F(u,H) = g(u) H ,
% with func_g_u = g(u) = G(u) - u * G'(u)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% V = F(u,H)
F_out = func_g_u .* H;