function MCFdiff_Ellipse_elong_runscript
% Runscript for 'MCF diff' code package,
% for the special case of an elongated ellipsoid.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = V \nu, with V given as below
flow = 'MCFdiff_basic';         % MCF: V = -F(u,H) with F(u,H) = g(u) H

T = 1;              % final time
m = 2;              % surface dimension

% time step size
tau=.2*2.^(-8);


%% loading mesh information
%%%%%%%%%% 
% elongated ellipsoid
surf_name = 'Ellipsoid_elong';
% distance function 
fd=@(x) x(:,1).^2/3^2 + x(:,2).^2/1^2 + x(:,3).^2/1^2 - 2^2;
grad_fd = @(x) [2 * x(:,1)/3^2 ...
                2 * x(:,2)/1^2 ...
                2 * x(:,3)/1^2];

%% loading P2 mesh
addpath('../aux_func');
[Nodes,Elements,Elements_plot] = load_mesh_p2(surf_name,fd);

dof=length(Nodes)

%% initial data
% normal vector
Normal = load_normal_p2(surf_name,Nodes,Elements_plot,fd,grad_fd);
length_Normal = sqrt(sum(Normal.^2, 2));

% mean curvature
[M,H_init] = surface_assembly_P2_H(Nodes,Elements, Normal);

r = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2 + Nodes(:,3).^2) ;
u_init = 1 * ( 10*ones(dof,1) - 30 ./ (r.^2+1) ) - 3;
[G_init,dG_init,~] = func_G_evals(u_init);
w_init = [ Normal -func_F( u_init,G_init,dG_init, H_init) ];

rmpath('../aux_func');

%% plot initial surface and initial data
figure('Position', [100, 50, 1280, 720]);
subplot(1,2,1)
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
colorbar
% hold on;
% quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% hold off;
axis('equal')
title('mesh, normal vector and $V_h(\cdot,0)$, $H_h(\cdot,0)$, or $u_h(\cdot,0)$','interpreter','latex')

subplot(1,2,2)
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
colorbar
% hold on;
% quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% hold off;
axis('equal')
title('mesh, normal vector and $V_h(\cdot,0)$, $H_h(\cdot,0)$, or $u_h(\cdot,0)$','interpreter','latex')

drawnow
disp('')

%% calling algorithm

% MCFdiff_solver(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);

%%%%%%%%%%%%
% plotting
%%%%%%%%%%%%
% % fig=figure('Position', [100, 50, 1280, 720]);
% % plt=0; % no plotting case, due to runscripts
% % plt=1; % only plotting
% plt=2; % video
% % plt=6; % frames
% 
% 
% limits = 1 * [-1 1; -1 1; -1 1]; % spherical
% % limits = [-1.35 1.35; -1.35 1.35; -1.75 1.75]; % fat dumbbell iMCF
% % limits = [-1 1; -1 1; -1.15 1.15]; % fat dumbbell MCFgen2
% view_angle = [1 0.82 0.42];
% % view_angle = [1 1 0];
% % view_angle = [1 0 0];
% skipping = 3;
% T0 = 0;     % starting time
% 
% % title_text = 'MCF diff';
% 
% % MCFgeneralised_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,u_init, plt,limits,view_angle,skipping,title_text,flow,alpha);

%% mass and energy conservation

%%
% beep;

end