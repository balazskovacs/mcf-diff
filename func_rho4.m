function rho4_out = func_rho4(Nodes,t,Radius,R0,R1)
% inhomogeneity for u
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% coordinates
x = Nodes(:,1);
y = Nodes(:,2);
z = Nodes(:,3);

% exact solution
% u = x.*y*exp(-t);

%% \diff R(t) = dtR
% dtR=(1-Radius/R1)*Radius;
rho4_out = -(x.*y.*exp(-t).*(6.*R1.*x.^2 - 4.*R1.*Radius.^4 - 12.*R1.*Radius.^2 + 6.*R1.*y.^2 + 6.*R1.*z.^2 + 5.*Radius.^5 - Radius.^3.*x.^2 - Radius.^3.*y.^2 - Radius.^3.*z.^2 + R1.*Radius.^2.*x.^2 + R1.*Radius.^2.*y.^2 + R1.*Radius.^2.*z.^2))./(R1.*Radius.^4);
end