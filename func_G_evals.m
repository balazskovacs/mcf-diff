function [G] = func_G_evals(u) % ,dG,ddG
% 
% For v = V \nu, with V = - F(u,H)
% where F(u,H) = g(u) H
% 
% Evaluations of G and its derivatives for further computations.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% mean curvature flow
% % G(u) = 1
% G = ones(length(u),1);
% % G'(u)
% dG = 0*G;
% % G''(u)
% ddG = 0*G;

%% a basic example
% % a large enough constant
% G_0 = 10;
% 
% % G(u) = G_0 + u .* log(u)
% G = G_0 + u .* log(u);
% % % G'(u)
% % dG = log(u) + 1;
% % % G''(u)
% % ddG = 1 ./ u;

%% MCFdiff_g example
% G(r) =  r^{-\alpha}
% g(r) =  (1+\alpha) r^{-\alpha}

alpha = 4;

% G(u) = r^{-\alpha}
G = u.^( -alpha );
% % G'(u) = (-\alpha) * r^{-\alpha -1}
% dG = (-alpha) * u.^( - alpha - 1);
% % G''(u)
% ddG = (-alpha) * ( - alpha - 1) * u.^( - alpha - 2);

%% MCFdiff - dumbbell - K. Eckert example
% % G(u) =  alpha * u - u * log(u)
% % g(u) =  u
% 
% alpha = 10;
% 
% % G(u) = r^{-\alpha}
% G = alpha .* u - u .* log(u);
% % % G'(u) = (-\alpha) * r^{-\alpha -1}
% % dG = alpha - log(u) - 1;
% % % G''(u)
% % ddG = -1 ./ u;


end