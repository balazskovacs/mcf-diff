function rho1_out = func_rho1(Nodes,t,Radius,R0,R1)
% inhomogeneity for velocity law
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% coordinates
x = Nodes(:,1);
y = Nodes(:,2);
z = Nodes(:,3);

% exact solution
u = x.*y*exp(-t);

%% \diff R(t) = dtR
% dtR = (1-Radius/R1) * Radius;
rho1_out = [ - x.*(Radius/R1 - 1) - (x.*(2*u - 20))/Radius^2 , ...
             - y.*(Radius/R1 - 1) - (y.*(2*u - 20))/Radius^2 , ...
             - z.*(Radius/R1 - 1) - (z.*(2*u - 20))/Radius^2];

end