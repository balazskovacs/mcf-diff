switch plt
    case 0
        % no plotting
    case -1 % video
        
        view_angle = [1 0.42 0.42];
        
%         t1=sgtitle(title_text);
%         set(t1,'Interpreter','latex','FontSize',16);
        
        trisurf(Elements_plot,x_new(:,2),x_new(:,1),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        colorbar;
%         hold on;
%         quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
%         hold off;
        t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
        axis('equal');
%         limits=[-0.75 0.75; -0.75 0.75; -1.1 1.1];
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)

        drawnow
        
        writeVideo(vidObj, getframe(fig));
    case -2 % video for cup
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,3),x_new(:,2),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); 
%             colorbar;
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        xlim([-1.1 1.1]); ylim([-1.1 0.1]); zlim([-1.1 1.1]); 
        t1=title('$\Gamma_h[\mathbf{x}]$ and $u_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        t1=zlabel(['time $t=',num2str(t_new),'$']); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left'); % 
        view([1 0 0])
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); 
            colorbar;
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        xlim([-1.1 1.1]); ylim([-1.1 1.1]); zlim([-1.1 0.1]); 
        t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
        set(t1,'Interpreter','latex','FontSize',16);
%         t1=ylabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
%         set(t1,'Interpreter','latex','FontSize',12); % ,'HorizontalAlignment', 'left'
        view([0 0 -1])

        drawnow
        
        writeVideo(vidObj, getframe(fig));
    case 1 % elongated ellipsoid
        
        view_angle = [0 0 1];
        
        
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,2),x_new(:,1),x_new(:,3),0)
        axis('equal');
        t1=ylabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,2),x_new(:,1),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); colorbar;
%         hold on;
%         quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
%         hold off;
        axis('equal');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)

        drawnow
    case 12 % elongated ellipsoid
%         plt_times = [0 0.5 1 1.5 2];
        plt_times = [0 2 4 6 7.5];
        if min(abs(plt_times-t_new)) < tau/4
            plt_count = plt_count + 1;
            view_angle = [0 0 1];


            subplot(1,5,plt_count)
            trisurf(Elements_plot,x_new(:,2),x_new(:,1),x_new(:,3),full(u_new(:,1)))
            axis('equal');
            t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
            set(t1,'Interpreter','latex','FontSize',14);
            t1=ylabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
            set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
%                 xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
            view(view_angle)
            drawnow
        end
    case 102
        if mod(t_new,0.1) <= tau
            plt_count = plt_count + 1;
            view_angle = [1 0.42 0.42];
            trisurf(Elements_plot,x_new(:,2),x_new(:,1),x_new(:,3),full(u_new(:,1)))
            axis('equal');
%             t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
%             set(t1,'Interpreter','latex','FontSize',14);
            t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
            set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
            view(view_angle)
            xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:));
            drawnow
            saveas(fig,['flipbooks/ellipsoid/',num2str(plt_count),'.png']);
        end
    case 2 % cup example
%         view_angle = [1 1 2];
        
        
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
        axis('equal');
        t1=ylabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',16);
        view([1 0 0])
        xlim([-5 0]);
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); colorbar;
%         hold on;
%         quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
%         hold off;
        axis('equal');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$\nu_h$ and $u_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view([1 1 2])
        
%         waitforbuttonpress
%         pause
        drawnow
    case 3 % dumbbell
        view_angle = [1 1 0.42];
        
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),0)
        axis('equal');
        t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); colorbar;
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        t1=title('$u_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)

        drawnow
%         pause
%         waitforbuttonpress
    case 31
        plt_times = [0 0.05 0.06 0.068];
        if min(abs(plt_times-t_new)) < tau/4
            plt_count = plt_count + 1;
            view_angle = [1 0 0];

            subplot(1,4,plt_count)
            trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
    %         shading('interp');
            axis('equal');
%             colorbar;
            limits=[-0.75 0.75; -0.75 0.75; -1.1 1.1];
            xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
            t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
            set(t1,'Interpreter','latex','FontSize',12); % ,'HorizontalAlignment', 'left'
            t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
            set(t1,'Interpreter','latex','FontSize',16);
            view(view_angle)

            drawnow
        end
    case 4 % cup blender
        view_angle = [1 0 0];
        
        t1=sgtitle(title_text);
        set(t1,'Interpreter','latex','FontSize',16);
        
        subplot(1,2,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3)) % , full(w_new(:,4))
        axis('equal'); 
        colorbar;
        t1=zlabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
        set(t1,'Interpreter','latex','FontSize',12,'HorizontalAlignment', 'left');
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
        xlim([-1.2 0]);
        t1=title('surface evolution');
        set(t1,'Interpreter','latex','FontSize',16);
        view(view_angle)
        
        subplot(1,2,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
%         shading('interp');
        axis('equal'); colorbar;
%         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
%         xlim([-1.2 0]);
        t1=title('$u_h$');
        set(t1,'Interpreter','latex','FontSize',16);
        view_angle = [1 1 -2];
        view(view_angle)

        drawnow
%         pause
%         waitforbuttonpress
case 41 % cup blender
        plt_times = [0 0.03 0.06 0.1];
        if min(abs(plt_times-t_new)) < tau/4
            plt_count = plt_count + 1;
            view_angle = [0 0 -1];
%             view_angle = [1 0 0];
        
            subplot(1,4,plt_count)
            trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),full(u_new(:,1)))
    %         shading('interp');
            axis('equal'); 
%             colorbar;
    %         xlim(limits(1,:)); ylim(limits(2,:)); zlim(limits(3,:)); 
            xlim([-1.1 1.1]); ylim([-1.1 1.1]); zlim([-1.1 0.1]); 
%             t1=title('$\Gamma_h[\mathbf{x}]$');
            t1=title(['$\Gamma_h[\mathbf{x}]$ and $u_h$']);
            set(t1,'Interpreter','latex','FontSize',16);
            t1=ylabel(['time $t=$',num2str(t_new)]); % $\nu$ and $H$
            set(t1,'Interpreter','latex','FontSize',12); % ,'HorizontalAlignment', 'left'
            view(view_angle)

            drawnow
        end
end