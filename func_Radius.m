function R_out=func_Radius(t,R0,R1)

%% R(t) = 1, i.e. fixed sphere
% R_out=1;

%%
% R_out=1+t;
% R_out=1-t/2;
% R_out=1+t^2/2;
% R_out=1+t^4;
% R_out=1+sin(2*pi*t)/4;

%% mean curvature flow 
% for a two diemansional sphere
% m=2;
% R_out=real(sqrt(R0^2-2*m*t));

%% R(t) = solution of logistic ODE
R_out = (R0 * R1) ./ ( R0*(1-exp(-t)) + R1*exp(-t) );
