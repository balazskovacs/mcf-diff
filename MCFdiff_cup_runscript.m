function MCFdiff_cup_runscript
% Runscript for 'MCF diff' code package,
% for the special case of a self intersecting flow of a cup shaped surface.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = V \nu, with V given as below
flow = 'MCFdiff_basic';         % MCF: V = -F(u,H) with F(u,H) = g(u) H

T = 0.1;              % final time
% T = 0.068;              % final time for dumbbell
m = 2;              % surface dimension

% time step size
tau = 10^(-3);


%% loading mesh information
%%%%%%%%%%
% cup (from blender)
surf_name = 'Cup';
% distance function 
fd = 'unknown';
grad_fd = 'too_complicated';


%% loading P2 mesh
addpath('../aux_func');
[Nodes,Elements,Elements_plot] = load_mesh_p2(surf_name,fd);

dof=length(Nodes)

%% initial data
one_vec = ones(dof,1);

% normal vector
Normal = load_normal_p2(surf_name,Nodes,Elements_plot,fd,grad_fd);
length_Normal = sqrt(sum(Normal.^2, 2));

% mean curvature
[M,H_init] = surface_assembly_P2_H(Nodes,Elements, Normal);

H_init(H_init>42) = 42;

%% initial value on blander cup
% pos = Nodes(:,3) < -1;
r = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2 + Nodes(:,3).^2);
r_xy = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2);
pos = r > 0.94 & Nodes(:,3) < -0.3;
u_add = zeros(dof,1);
% u_add(pos,1) = -9;
% u_add = -2+2*r.^0.5;
% u_add_in(~pos_out,1) = 0;
u_add(pos,1) =  exp( - 10 * r_xy(pos,1).^10 ).^1;
u_init = 10 * one_vec - 9 * u_add;


[g_u_init,~] = func_g_dg(u_init);
w_init = [ Normal -func_F( g_u_init , H_init) ];

min(w_init(:,4))
max(w_init(:,4))

rmpath('../aux_func');

%% plot initial surface and initial data
% figure('Position', [100, 50, 1280, 720]);
% % subplot(1,3,1)
% % % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% % % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
% % % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
% % colorbar
% % hold on;
% % % quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% % % hold off;
% % axis('equal')
% % title('mesh and $H_h(\cdot,0)$','interpreter','latex')
% 
% subplot(1,2,1)
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
% colorbar; 
% % colormap(hsv);
% % hold on;
% % quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% % hold off;
% axis('equal')
% % xlim([-1.5 0]);
% title('mesh and $u_h(\cdot,0)$','interpreter','latex')
% view([1 1 -2])
% 
% subplot(1,2,2)
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
% colorbar; 
% % colormap(hsv);
% % hold on;
% % quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% % hold off;
% axis('equal')
% title('mesh and $V_h(\cdot,0)$','interpreter','latex')
% view([1 1 -2])
% 
% drawnow
% disp('')


%% calling algorithm

% MCFdiff_solver(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);

MCFdiff_plotter(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);
% MCFdiff_conservation_plotter(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);



%%%%%%%%%%%%
% plotting
%%%%%%%%%%%%
% % fig=figure('Position', [100, 50, 1280, 720]);
% % plt=0; % no plotting case, due to runscripts
% % plt=1; % only plotting
% plt=2; % video
% % plt=6; % frames
% 
% 
% limits = 1 * [-1 1; -1 1; -1 1]; % spherical
% % limits = [-1.35 1.35; -1.35 1.35; -1.75 1.75]; % fat dumbbell iMCF
% % limits = [-1 1; -1 1; -1.15 1.15]; % fat dumbbell MCFgen2
% view_angle = [1 0.82 0.42];
% % view_angle = [1 1 0];
% % view_angle = [1 0 0];
% skipping = 3;
% T0 = 0;     % starting time
% 
% % title_text = 'MCF diff';
% 
% % MCFgeneralised_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,u_init, plt,limits,view_angle,skipping,title_text,flow,alpha);

%% mass and energy conservation

%%
% beep;

end