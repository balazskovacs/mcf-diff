function MCFdiff_runscript_convtest
% 
% Runscript MCF diff.
%
% Required inputs: 
%   - quadratic initial triangulation
%   - initial values are based on normal and mean curvature
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% format long;
format short;

k=2;                % BDF order
T = 1;              % final time
m = 2;              % surface dimension


% v = V \nu, with V given as below
% V = -F(u,H) with F(u,H) = g(u) H, i.e. V = - g(u) H
% flow = 'MCFdiff_basic';         

R0 = 1;             % initial surface radius
R1 = 2;             % final surface radius (at time T)

flow = 'MCFdiff_g';         
u0 = 1;             % initial value for constant concentration
alpha = 2 ;          % parameter in g(u) =  (1+\alpha) u^{-\alpha}


% spatial refinements
n_vect = (6:6);
% temporal refinements
tau_vect = .2*2.^(-10:-1:-10);

%%%%%%%%%% 
% sphere (max n = 0:7)
surf_name = 'Sphere';
fd=@(x) x(:,1).^2 + x(:,2).^2 + x(:,3).^2 - 1;
% gradient of distance function
grad_fd = @(x) [2 * x(:,1) ...
                2 * x(:,2) ...
                2 * x(:,3)];

save_text = surf_name;

%% adding path of auxiliary functions
addpath('../aux_func');

%% test loops
% loop over mesh sizes
for in=1:length(n_vect)
    n=n_vect(in);
    
    %% loading mesh
    [Nodes,Elements,Elements_plot] = load_num_mesh_p2(surf_name,n,fd,grad_fd);
    % DO NOT scale to any radius
    % the input sphere should be always of radius 1
    
    DOF = length(Nodes)
    
    %% loop over timestep sizes
    for jtau=1:length(tau_vect)
        tau = tau_vect(jtau)
        
%         MCFdiff_solver_convtest(Nodes,Elements,Elements_plot,k,n,tau,m,R0,R1,T, flow);
        
        % g(r) =  (1+\alpha) r^{-\alpha} version
        MCFdiff_g_solver_convtest(Nodes,Elements,Elements_plot,k,n,tau,m,u0,R0,alpha,T, flow);
% %         MCFdiff_g_solver_no_u_test(Nodes,Elements,Elements_plot,k,n,tau,m,u0,R0,alpha,T, flow);
	
    end
end

%% removing path of auxiliary functions
rmpath('../aux_func');

%%
beep;

end