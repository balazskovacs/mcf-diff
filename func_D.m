function D_out = func_D(u)
% 
% D(u) = m(u) G''(u), with 
% G'' given
% and mobility m(u) = u
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% a basic example such that 
D_out = ones(size(u));

end