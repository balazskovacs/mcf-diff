switch plt
    case 0
        % empty
    case 1
        %% 1 -- plotting on the fly
        subplot(1,4,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),one_vec);
        title('$\Gamma_h[x]$')
        zlabel(['time $t=',num2str(t_new),'$'])
        axis equal;

        subplot(1,4,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),one_vec);
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
        hold off;
        title('$\nu_h$')
        axis equal;

        subplot(1,4,3)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,4));
        title('$V_h$')
        axis equal;
        colorbar;

        subplot(1,4,4)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),u_new);
        title('$u_h$')
        axis equal;
        colorbar;

        drawnow

        % pause
    case 2
        %% 2 -- plotting on the fly
        subplot(2,4,1)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),one_vec);
        title('$\Gamma_h[x]$')
        zlabel(['time $t=',num2str(t_new),'$'])
        axis equal;

        subplot(2,4,2)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),one_vec);
        hold on;
        quiver3(x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,1),w_new(:,2),w_new(:,3))
        hold off;
        title('$\nu_h$')
        axis equal;

        subplot(2,4,3)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),w_new(:,4));
        title(['$V_h = ',num2str(mean(w_new(:,4))),'$'])
        axis equal;
        colorbar;

        subplot(2,4,4)
        trisurf(Elements_plot,x_new(:,1),x_new(:,2),x_new(:,3),u_new);
        title(['$u_h = ',num2str(mean(u_new)),'$'])
        axis equal;
        colorbar;
    
        
        Radius = func_Radius_g(t_new,R0,u0);
        x_exact = Radius * Nodes0;
        u_exact = func_solution_g(u0,R0,Radius) * one_vec;
        H_exact = func_sphere_curvature(x_exact,Radius,m);
        w_exact = [func_sphere_normal(x_exact) -(1+alpha)*u_exact.^(-alpha) .* H_exact];
    
        subplot(2,4,1+4)
        trisurf(Elements_plot,x_exact(:,1),x_exact(:,2),x_exact(:,3),one_vec);
        title('$\Gamma[X]$')
        zlabel(['time $t=',num2str(t_new),'$'])
        axis equal;

        subplot(2,4,2+4)
        trisurf(Elements_plot,x_exact(:,1),x_exact(:,2),x_exact(:,3),one_vec);
        hold on;
        quiver3(x_exact(:,1),x_exact(:,2),x_exact(:,3),w_exact(:,1),w_exact(:,2),w_exact(:,3))
        hold off;
        title('$\nu$')
        axis equal;

        subplot(2,4,3+4)
        trisurf(Elements_plot,x_exact(:,1),x_exact(:,2),x_exact(:,3),w_exact(:,4));
        title(['$V = ',num2str(w_exact(1,4)),'$'])
        axis equal;
        colorbar;

        subplot(2,4,4+4)
        trisurf(Elements_plot,x_exact(:,1),x_exact(:,2),x_exact(:,3),u_exact);
        title(['$u = ',num2str(u_exact(1,1)),'$'])
        axis equal;
        colorbar;

        drawnow

%         pause
end