function rho3_out = func_rho3(Nodes,t,Radius,R0,R1)
% inhomogeneity for V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% coordinates
x = Nodes(:,1);
y = Nodes(:,2);
z = Nodes(:,3);

% exact solution
u = x.*y.*exp(-t);

%% \diff R(t) = dtR
% dtR=(1-Radius/R1)*Radius;
rho3_out = - (2.*(Radius./R1 - 1))./Radius - (4.*x.*y.*exp(-t) - 40)./Radius.^3 - (12.*x.*y.*exp(-t).*(x.^2 - 2.*Radius.^2 + y.^2 + z.^2))./Radius.^5;

end