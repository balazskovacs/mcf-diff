function nrml = func_sphere_normal(Nodes)

%% (outward) normal vector for any sphere
nrm = sqrt(Nodes(:,1).^2+Nodes(:,2).^2+Nodes(:,3).^2);
% nrml = [Nodes(:,1)./nrm Nodes(:,2)./nrm Nodes(:,3)./nrm];
nrml = Nodes ./ nrm;