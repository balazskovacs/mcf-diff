function H_out = func_K(u,G,dG,V)
% 
% For v = V \nu, with V = - F(u,H)
% where F(u,H) = g(u) H
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% g(u) = G(u) - u * G'(u)
func_g_u = G - u .* dG;

% H = K(u,V)
H_out = func_g_u.^(-1) .* V;