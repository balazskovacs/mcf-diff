function MCFdiff_solver(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, varargin)
%
% This code is the main solver of 'MCF diff', 
% it numerically solves the flow coupling mean curvature flow and
% diffusion on a closed surface.
% The code generates and saves data for the plots and videos,
% generated using 'MCFdiff_plotter.m'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

plt=41;

fig = figure('position',[10 150 1200 500]);

%% initial surface

% degrees of freedom
dof = length(Nodes);
% constant 1 vector
one_vec = ones(dof,1);

% saving initial mesh
Nodes0 = Nodes;

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initial values
t = [];
t_new = 0;
t = [t t_new];
x_past(:,:,1) = Nodes0;
w_past(:,:,1) = w_init;
u_past(:,1) = u_init;

M = surface_assembly_P2_mass(x_past(:,:,1),Elements);
Mu_past(:,1) = M * u_init;

%
x_new = Nodes0;
w_new = w_init;
u_new = u_init;
plt_count = 0;
% plt=0;
limits=2*[-3 3; -1 1; -1 1]; %x-y-z limits
skipping = 1;
title_text = [ '\verb|',flow,'|' ];
plotter_script;
%     pause
%     waitforbuttonpress;


for k_ind = 1:k-1
    
    % BDF coefficients
    [delta,gamma] = BDF_tableau(k_ind);
    
    t_new = k_ind*tau;
    
    % extrapolations
    x_tilde=zeros(dof,3);
    w_tilde=zeros(dof,4);
    u_tilde=zeros(dof,1);
    for ind=1:k_ind
        % surface
        x_tilde=x_tilde+gamma(k_ind-ind+1)*x_past(:,:,end-k_ind+ind);
        % dynamic variables w = (nu,V)
        w_tilde=w_tilde+gamma(k_ind-ind+1)*w_past(:,:,end-k_ind+ind);
        % diffusion variable u
        u_tilde = u_tilde + gamma(k_ind-ind+1) * u_past(:,end-k_ind+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    w_tilde(:,1:3) = w_tilde(:,1:3) ./ sqrt( sum(w_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP PRE
    % contributions from the past
    dx = zeros(dof,3);
    dw = zeros(dof,4);
    du = zeros(dof,1);
    dMu = zeros(dof,1);
    for ind = 1:k_ind
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        dw = dw + delta(ind+1) * w_past(:,:,end-ind+1);
        du = du + delta(ind+1) * u_past(:,end-ind+1);
        dMu = dMu + delta(ind+1) * Mu_past(:,end-ind+1);
    end
    
    %%%%%%%%%%%%%%%%
    % STEP (1) - Solve the diffusion equation 
    % (Needs: M, Axu)
    [M,A , Mxuw,Axu , f,f_nu] = surface_assembly_P2_MCFdiff_no_matu(x_tilde,Elements, w_tilde , u_tilde);
    
    % solving the linear system for the diffusion variable
    u_new = ( delta(1) * M + tau * Axu ) \ ( -dMu );
    Mu_new = M * u_new;
    
    % approximation of the material derivative \mat_h u_h
    % for STEP (2)
    mat_u = (1/tau) * (delta(1) * u_new + du);
    
    %%%%%%%%%%%%%%%%
    % STEP (2) - Solve the geometric evolution equations w = (nu,V)
    % (Needs: Mxwu, A, f)
    % surface FEM assembly on the EXTRAPOLATED surface
    [ f_V ] = surface_assembly_P2_MCFdiff_matu(x_tilde,Elements, w_tilde , u_tilde, mat_u);
        
    % solving the linear system for the geometric variable w = (nu,V)
    w_new = ( delta(1)*Mxuw + tau*A ) \ ( tau * (f + f_nu + f_V ) - Mxuw * dw );
    
    %%%%%%%%
%     % projecting nu_h onto the unit sphere
%     w_new(:,1:3) = w_new(:,1:3) ./ sqrt( sum(w_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP (3) and (4) determining the velocity and integrating the surface
    % (Needs: w_new, dx)
    
    % computing new velocity
%     V_new = w_new(:,4); nu_new = u_new(:,1:3);    
%     v_new = V_new .* nu_new;
    % rho_x r.h.s. for the surface position ODE
%     rho_x = tau * v_new - dx;
%     rho_x = tau * w_new(:,4) .* w_new(:,1:3) - dx;

    % computing new surface positions
    x_new = (1/delta(1)) * ( tau * ( w_new(:,4) .* w_new(:,1:3) ) - dx );
    
    %%%%%%%%%%%%%%%%
    % STEP POST
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k_ind+1)=x_new;
    % new dynamic variables
    w_past(:,:,k_ind+1)=w_new;
    % new diffusion variable
    u_past(:,k_ind+1) = u_new;
    Mu_past(:,k_ind+1) = Mu_new;
    
    % new time
    t=[t t_new];
    
    % compute mass and energy
    
    % plotting on the fly
%     plt=2;
    limits=2*[-3 3; -1 1; -1 1]; %x-y-z limits
    skipping = 1;
    title_text = [ '\verb|',flow,'|' ];
    plotter_script;
%     pause
end


%% BDF coefficients
[delta,gamma] = BDF_tableau(k);

%% time Integration
for steps = k:ceil(T/tau)
    % timestep
    t_new = t(end) + tau
    
    % extrapolations
    x_tilde=zeros(dof,3);
    w_tilde=zeros(dof,4);
    u_tilde=zeros(dof,1);
    for ind=1:k
        % surface
        x_tilde=x_tilde+gamma(k-ind+1)*x_past(:,:,end-k+ind);
        % dynamic variables u = (nu,V)
        w_tilde=w_tilde+gamma(k-ind+1)*w_past(:,:,end-k+ind);
        % diffusion variable u
        u_tilde = u_tilde + gamma(k-ind+1) * u_past(:,end-k+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    w_tilde(:,1:3) = w_tilde(:,1:3) ./ sqrt( sum(w_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP PRE
    % contributions from the past
    dx = zeros(dof,3);
    dw = zeros(dof,4);
    du = zeros(dof,1);
    dMu = zeros(dof,1);
    for ind = 1:k
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        dw = dw + delta(ind+1) * w_past(:,:,end-ind+1);
        du = du + delta(ind+1) * u_past(:,end-ind+1);
        dMu = dMu + delta(ind+1) * Mu_past(:,end-ind+1);
    end
    
    %%%%%%%%%%%%%%%%
    % STEP (1) - Solve the diffusion equation 
    % (Needs: M, Axu)
    [M,A , Mxuw,Axu , f,f_nu] = surface_assembly_P2_MCFdiff_no_matu(x_tilde,Elements, w_tilde , u_tilde);
    
    % solving the linear system for the diffusion variable
    u_new = ( delta(1) * M + tau * Axu ) \ ( -dMu );
    Mu_new = M * u_new;
    
    % approximation of the material derivative \mat_h u_h
    % for STEP (2)
    mat_u = (1/tau) * (delta(1) * u_new + du) ;
    
    %%%%%%%%%%%%%%%%
    % STEP (2) - Solve the geometric evolution equations w = (nu,V)
    % (Needs: Mxwu, A, f)
    % surface FEM assembly on the EXTRAPOLATED surface
    [ f_V ] = surface_assembly_P2_MCFdiff_matu(x_tilde,Elements, w_tilde , u_tilde, mat_u);
        
    % solving the linear system for the geometric variable w = (nu,V)
    w_new = ( delta(1)*Mxuw + tau*A ) \ ( tau * (f + f_nu + f_V ) - Mxuw * dw );
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
%     w_new(:,1:3) = w_new(:,1:3) ./ sqrt( sum(w_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP (3) and (4) determining the velocity and integrating the surface
    % (Needs: w_new, dx)
    
    % computing new velocity
%     V_new = w_new(:,4); nu_new = u_new(:,1:3);    
%     v_new = V_new .* nu_new;
    % rho_x r.h.s. for the surface position ODE
%     rho_x = tau * v_new - dx;
%     rho_x = tau * w_new(:,4) .* w_new(:,1:3) - dx;

    % computing new surface positions
    x_new = (1/delta(1)) * ( tau * ( w_new(:,4) .* w_new(:,1:3) ) - dx );
    
    %%%%%%%%%%%%%%%%
    % STEP POST
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k+1)=x_new;
    x_past(:,:,1)=[];
    % new dynamic variables
    w_past(:,:,k+1)=w_new;
    w_past(:,:,1)=[];
    % new diffusion variable
    u_past(:,k+1)=u_new;
    u_past(:,1)=[];
    Mu_past(:,k+1) = Mu_new;
    Mu_past(:,1)=[];
    
    % new time
    t=[t t_new];
    
    %% plotting on the fly
%     plt=2;
    limits=2*[-3 3; -1 1; -1 1]; %x-y-z limits
    skipping = 1;
    title_text = [ '\verb|',flow,'|' ];
    plotter_script;
%     pause
%     waitforbuttonpress;
    
    %% compute mass and energy
    
    %% save data
%     if min(abs((0:0.01:T)-t_new)) < tau/4
        % for plotting: the current solution
        s1.t_new = t_new;
        s1.x_new = x_new;
        s1.w_new = w_new;
    %     s1.H_new = H_new;
        s1.u_new = u_new;
        save(['results/MCFdiff',varargin{:},'/',flow,'_solution_at_t',num2str(t_new),'_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s1');

        % for reruns: saving the past of surfaces and variables as a single 'mat' file
%         s2.t_new = t_new;
%         s2.x_past = x_past;
%         s2.w_past = w_past;
%         save(['results/MCFgen_test',varargin{:},'/',flow,num2str(alpha),'_past','_BDF',num2str(k),'_tau',num2str(tau),'.mat'],'-struct','s2')
%     end
end

% disp('')

