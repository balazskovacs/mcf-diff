function [pa1_K_out , pa2_K_out] = func_partials_K(func_g_u,func_dg_u,V)
% 
% For v = V \nu, with V = - F(u,H)
% where F(u,H) = g(u) H and
% func_g_u = g(u) = G(u) - u * G'(u),
% func_dg_u = g'(u) = - u * G''(u)
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \partial_1 K(u,V) = (-1)*(g(u))^(-2) * g'(u) * V
pa1_K_out = (-1) .* func_g_u.^(-2) .* func_dg_u .* V;

% \partial_2 K(u,V) = 1/g(u)
pa2_K_out = func_g_u.^(-1);