function [g_u,dg_u] = func_g_dg(u) % ,dG,ddG
% 
% Evaluations of g and g' for further computations.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% mean curvature flow
% % G(u) = 1
% G = ones(length(u),1);
% % G'(u)
% dG = 0*G;
% % G''(u)
% ddG = 0*G;

%% a basic example g(u) = G_0 - u
% % a large enough constant
% G_0 = 10;
% 
% % for G(u) = G_0 + u .* log(u)
% % g(u) = G(u) - u * G'(u)
% g_u = G_0 - u;
% % g'(u) = - u * G''(u)
% dg_u = - ones(size(u));

%% MCFdiff_g example g(r) =  (1+\alpha) r^{-\alpha}
% G(r) =  r^{-\alpha}
% g(r) =  (1+\alpha) r^{-\alpha}

alpha = 4;

% G(u) = r^{-\alpha}
% for G(u) = r^{-\alpha}
% g(u) = G(u) - u * G'(u)
g_u = (1+alpha) .* u.^( - alpha );
% g'(u) = - u * G''(u)
dg_u = - alpha * (1+alpha) * u.^( - alpha - 1);

%% MCFdiff g(u) =  u -  (for K. Eckert dumbbell example)
% % G(u) =  \alpha * u - u * log(u)
% % g(u) =  u
% 
% % for G(u) = alpha * u - u * log(u)
% % g(u) = G(u) - u * G'(u)
% g_u = u;
% % g'(u) = - u * G''(u)
% dg_u = ones(size(u));



end