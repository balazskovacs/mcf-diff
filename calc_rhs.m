function calc_rhs
%% ----- calculate r.h.s. function - f -----
syms t;
syms x y z;
syms a dta;
syms R1 Radius;

%% ----- true solution -----
% u = x * y;
u = exp(-t) * x * y;

% computing forcing terms for MCF (with u from above)
% we set: G(u) = G_0 + u .* log(u) with G_0 = 10
% g(u) = G(u) - u * G'(u)
func_g_u = 10 - u; % = G_0 + u .* log(u) - u * ( log(u) + 1 );
% g'(u) = - u * G''(u)
func_dg_u = - 1 ; % = - u * 1/u;
% func_ddG_u = 1/u;
% func_mobility = u;
func_D = 1; % func_mobility * func_ddG_u;

%% normal velocity
% V = - g(u) H
V = - func_g_u * 2/Radius;

%% ----- surface normal vecor (nu) -----
% fixed sphere
% nu=[x;y;z];

% sphere of radius Radius=R(t)
nu = [x;y;z] / Radius;

% bouncing ellipsoid
% nu=[a^(-1)*x;y;z]/sqRadius(a^(-2)*x^2+y^2+z^2);
% nrm=simple(nu(1)^2+nu(2)^2+nu(3)^2)

%% ----- surface velocity (v) -----
% fixed sphere
% v=[0;0;0];

% sphere of radius R(t) = 1 + t
% v=(1/Radius)*[x;y;z];
% sphere of radius R(t) = 1 + t^2
% v=(2*t/Radius)*[x;y;z];

% sphere of radius R(t) satisfying logistic ODE
v=(1-Radius/R1)*[x;y;z];

% bouncing ellipsoid
% v=[x^2*a^(-2)*dta/sqRadius(4*a^(-2)*x^2+4*y^2+4*z^2);0;0]

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inhomogeneity for velocity law
% \varrho_1
rho1_sym = simplify( v + func_g_u * 2 / Radius^2 * [x;y;z] );
rho1 = vectorize( rho1_sym );
rho1

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inhomogeneity for \nu
% surface gradient of u
grad_u = [ diff(u,x) ; diff(u,y) ; diff(u,z)];
nbg_u = grad_u - ( grad_u(1)*nu(1) + grad_u(2)*nu(2) + grad_u(3)*nu(3) ) * nu;
% \varrho_2
rho2 = simplify( - func_dg_u / func_g_u * 2 / Radius * nbg_u);
rho2 = vectorize( rho2 );
rho2

% rho2_2 = simplify( func_dg_u / func_g_u^2 * V * nbg_u);
% rho2_2 = vectorize( rho2_2 );
% rho2_2

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inhomogeneity for V
% material derivative of u: \mat u = \pa_t u + v \cdot grad u 
mat_u = simplify( diff(u,t) + ( grad_u(1)*v(1) + grad_u(2)*v(2) + grad_u(3)*v(3) ) );
% Laplace--Beltrami of g(u)
LB_func_g_u = LaplaceBeltrami(func_g_u,nu,1);
% \varrho_3
rho3 = simplify( 2 / Radius * ( 1 - Radius/R1) ...
        + LB_func_g_u * 2 / Radius ...
        + func_g_u * 4 / Radius^3 );
rho3 = vectorize( rho3 );
rho3

% % alternative version
% grad_g_u = [ diff(func_g_u,x) ; diff(func_g_u,y) ; diff(func_g_u,z)];
% mat_g_u = simplify( diff(V,t) + ( grad_g_u(1)*v(1) + grad_g_u(2)*v(2) + grad_g_u(3)*v(3) ) );
% LB_V = LaplaceBeltrami(V,nu,1);
% rho3_2 = simplify( 2/Radius * (1 - Radius/R1) - 1/func_g_u * 2/Radius * mat_g_u - LB_V - 2/Radius^2 * V - func_dg_u / func_g_u^2 * V * mat_u );
% rho3_2 = vectorize( rho3_2 );
% rho3_2

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% diffusion on the surface

% %%%%%%%%
% % setting v = V \nu + rho_1
% v = v + rho1_sym;
% %%%%%%%%

% material derivative of u: \mat u = \pa_t u + v \cdot grad u 
mat_u = simplify( diff(u,t) + ( grad_u(1)*v(1) + grad_u(2)*v(2) + grad_u(3)*v(3) ) );

% transport term: u \Nabla_{\Ga} \cdot v
gradv1=[ diff(v(1),x) ; diff(v(1),y) ; diff(v(1),z)];
gradv2=[ diff(v(2),x) ; diff(v(2),y) ; diff(v(2),z)];
gradv3=[ diff(v(3),x) ; diff(v(3),y) ; diff(v(3),z)];
u_div_Ga_v = u * (gradv1(1)+gradv2(2)+gradv3(3)-(gradv1(1)*nu(1)+gradv1(2)*nu(2)+gradv1(3)*nu(3))*nu(1)-(gradv2(1)*nu(1)+gradv2(2)*nu(2)+gradv2(3)*nu(3))*nu(2)-(gradv3(1)*nu(1)+gradv3(2)*nu(2)+gradv3(3)*nu(3))*nu(3));

% Laplace--Beltrmi term \div_Ga (G''(u) \nbg u)
LB_term = - LaplaceBeltrami(u,nu,func_D);

% inhomogeneity for surface PDE \varrho_4
rho4 = simplify(mat_u + u_div_Ga_v + LB_term);
rho4 = vectorize( rho4 );
rho4
 

%% div_\Ga id
% v=[x;y;z];
% gradv1=[ diff(v(1),x) ; diff(v(1),y) ; diff(v(1),z)];
% gradv2=[ diff(v(2),x) ; diff(v(2),y) ; diff(v(2),z)];
% gradv3=[ diff(v(3),x) ; diff(v(3),y) ; diff(v(3),z)];
% div_x=simplify(gradv1(1)+gradv2(2)+gradv3(3)-(gradv1(1)*nu(1)+gradv1(2)*nu(2)+gradv1(3)*nu(3))*nu(1)-(gradv2(1)*nu(1)+gradv2(2)*nu(2)+gradv2(3)*nu(3))*nu(2)-(gradv3(1)*nu(1)+gradv3(2)*nu(2)+gradv3(3)*nu(3))*nu(3))
end


function LB_u = LaplaceBeltrami(u,nu,Au)
% with weighting

% symbolic variables
syms t;
syms x y z;
syms a dta;
syms R1 Radius;

%% Laplace_{\Ga} u
% \nbg u
grad_u = [ diff(u,x) ; diff(u,y) ; diff(u,z)];
nbg_u = grad_u-(grad_u(1)*nu(1)+grad_u(2)*nu(2)+grad_u(3)*nu(3))*nu;
% div_\Ga nbg_u
gradw1 = Au * [ diff(nbg_u(1),x) ; diff(nbg_u(1),y) ; diff(nbg_u(1),z)];
gradw2 = Au * [ diff(nbg_u(2),x) ; diff(nbg_u(2),y) ; diff(nbg_u(2),z)];
gradw3 = Au * [ diff(nbg_u(3),x) ; diff(nbg_u(3),y) ; diff(nbg_u(3),z)];
LB_u = gradw1(1) + gradw2(2) + gradw3(3) ...
     - (gradw1(1)*nu(1)+gradw1(2)*nu(2)+gradw1(3)*nu(3))*nu(1)...
     - (gradw2(1)*nu(1)+gradw2(2)*nu(2)+gradw2(3)*nu(3))*nu(2)...
     - (gradw3(1)*nu(1)+gradw3(2)*nu(2)+gradw3(3)*nu(3))*nu(3);

LB_u = simplify( LB_u );

end