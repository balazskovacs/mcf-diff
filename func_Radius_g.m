function R_out = func_Radius_g(t,R0,u0,alpha)
% g(r) =  (1+\alpha) r^{-\alpha}
% 
% R(t) = \Big( R_0^{2 - \alpha m} - t b (2 - \alpha m) \Big)^{\frac{1}{2 - \alpha m}}
% with R(0) = R_0
% and b = m \, (1+\alpha) \Big( u_0 \, R_0^m \Big)^{-\alpha}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% dimension
m = 2;

b = m * (1+alpha) * ( u0 * R0^m )^(-alpha);

D = ( 2 - alpha*m );

%% R(t) = solution of logistic ODE
R_out = ( R0^D - t * b * D ).^(1/D);
