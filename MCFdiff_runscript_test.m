function MCFdiff_runscript_test
% 
% Main driver file for the convergence testing of the 'MCF diff' code package.
% 
% Written for the paper: 
% C. M. Elliott, H. Garcke, and B. Kov�cs.
% Numerical analysis for the interaction of mean curvature flow and
% diffusion on closed surfaces. February 2022.
% 
% The algorithm solves a special case of the system coupling 
% mean curvature flow interacting with diffusion, where the exact solution
% is known. 
% The algorithm uses quadratic evolving surface finite element in space
% and a BDF method in time.
%
% 
% 
% Copyright (c) 2022, Bal�zs Kov�cs (balazs.kovacs@ur.de) 
% under the GNU General Public License, version 3.
% 
% If you use 'MCF diff' in any program, project, or publication, please acknowledge
% its authors by adding a reference to the above publication.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

format long;

k=2;
T=10;
T0=5;
m=2;
R0=1;
R1=2;
flow = 'MCFdiff_g';

% spatial refinements
n_vect=(0:7);
% temporal refinements
tau_vect=.2*2.^(-0:-1:-7);

%% test loops
% loop over mesh sizes
for in=1:length(n_vect)
    n=n_vect(in);
    %% mesh and preprocessing
    if exist(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt'], 'file')==2
        Nodes=load(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt']);
        Elements=load(['../surfs/Sphere_elements_p2_',num2str(n),'.txt']);
        Elements_plot=load(['../surfs/Sphere_elements_plot_p2_',num2str(n),'.txt']);
        
%         figure
%         trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3))
    else
        Nodes=load(['../surfs/Sphere_nodes',num2str(n),'.txt']);
        Elements=load(['../surfs/Sphere_elements',num2str(n),'.txt']);
        [~,~,Nodes,Elements,Elements_plot]=preprocess(Nodes,Elements,2);
        % ONLY for SPHERE: norming back to unit length
        nrm=sqrt(Nodes(:,1).^2+Nodes(:,2).^2+Nodes(:,3).^2);
        Nodes(:,1)=Nodes(:,1)./nrm;
        Nodes(:,2)=Nodes(:,2)./nrm;
        Nodes(:,3)=Nodes(:,3)./nrm;
        save(['../surfs/Sphere_nodes_p2_',num2str(n),'.txt'], 'Nodes', '-ASCII');
        save(['../surfs/Sphere_elements_p2_',num2str(n),'.txt'], 'Elements', '-ASCII');
        save(['../surfs/Sphere_elements_plot_p2_',num2str(n),'.txt'], 'Elements_plot', '-ASCII');
        
        figure
        trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3))
        drawnow
    end
    
    dof=length(Nodes)
    
    %% assembly of the evolving stiffness and mass matrix on the EXTRAPOLATED surface
%     [A,M]=surface_assembly_P2(Nodes,Elements);

    %% loop over timestep sizes
    for jtau=1:length(tau_vect)
        tau=tau_vect(jtau)
        %% MCF diff convergence test
        MCFdiff_solver_convtest(Nodes,Elements,Elements_plot,k,n,tau,m,R0,R1,T, flow)
    end
end

beep;

% convplots_fig_MCF_forced;