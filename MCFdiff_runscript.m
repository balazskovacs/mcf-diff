function MCFdiff_runscript
% 
% Main driver file for 'MCF diff' code package.
% 
% Written for the paper: 
% C. M. Elliott, H. Garcke, and B. Kov�cs.
% Numerical analysis for the interaction of mean curvature flow and
% diffusion on closed surfaces. February 2022.
% 
% The algorithm for mean curvature flow interacting with diffusion uses
% quadratic evolving surface finite element in space
% and a BDF method in time.
%
% Required inputs: 
%   - initial surface parametrisation
%   - initial values are based on normal and mean curvature
%   - time interval, stepsizes, meshes, etc.
% 
% Copyright (c) 2022, Bal�zs Kov�cs (balazs.kovacs@ur.de) 
% under the GNU General Public License, version 3.
% 
% If you use 'MCF diff' in any program, project, or publication, please acknowledge
% its authors by adding a reference to the above publication.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

k=2;                % BDF order

% v = V \nu, with V given as below
flow = 'MCFdiff_basic';         % MCF: V = -F(u,H) with F(u,H) = g(u) H

T = 1;              % final time
m = 2;              % surface dimension

% time step size
tau = 10^(-3);


%% loading mesh information
%%%%%%%%%% 
% % Sphere
% surf_name = 'Sphere';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + x(:,3).^2 - 1;
% grad_fd = @(x) [2 * x(:,1) ...
%                 2 * x(:,2) ...
%                 2 * x(:,3)];

%%%%%%%%%% 
% elongated ellipsoid
surf_name = 'Ellipsoid_elong';
% distance function 
fd=@(x) x(:,1).^2/3^2 + x(:,2).^2/1^2 + x(:,3).^2/1^2 - 2^2;
grad_fd = @(x) [2 * x(:,1)/3^2 ...
                2 * x(:,2)/1^2 ...
                2 * x(:,3)/1^2];
T = 7.5; 

%%%%%%%%%% 
% % Cup: intersecting implicit surfaces torii
% surf_name = 'Cup';
% % distance function 
% r = 0.2;
% ell=0; R = cos(ell*pi/8); 
% fd0=@(x) ( (x(:,1).^2 + x(:,2).^2).^(0.5) - R ).^2 + (x(:,3)+sin(ell*pi/8)).^2 - r.^2;
% ell=1; R = cos(ell*pi/8);
% fd1=@(x) ( (x(:,1).^2 + x(:,2).^2).^(0.5) - R ).^2 + (x(:,3)+sin(ell*pi/8)).^2 - r.^2;
% ell=2; R = cos(ell*pi/8);
% fd2=@(x) ( (x(:,1).^2 + x(:,2).^2).^(0.5) - R ).^2 + (x(:,3)+sin(ell*pi/8)).^2 - r.^2;
% ell=3; R = cos(ell*pi/8);
% fd3=@(x) ( (x(:,1).^2 + x(:,2).^2).^(0.5) - R ).^2 + (x(:,3)+sin(ell*pi/8)).^2 - r.^2;
% fdS=@(x) x(:,1).^2 + (x(:,2)).^2 + (x(:,3)+0.9).^2 - (r*1.5).^2;
% fd=@(x) fd0(x) .* fd1(x) .* fd2(x) .* fd3(x) .* fdS(x) - 0.05^2;% .* fd2(x) .* fdS(x);
% grad_fd = 'too_complicated';

%%%%%%%%%%
% % blender cup
% surf_name = 'Cup';
% % distance function 
% fd = 'unknown';
% grad_fd = 'too_complicated';
% 
% T = 0.1;              % final time for blender cup

%%%%%%%%%% 
% % a dumbbell (Elliott--Styles)
% % surf_name = 'Dumbbell2';
% % % distance function 
% % fd=@(x) x(:,1).^2 + x(:,2).^2 + 2*(x(:,3).^2).*(x(:,3).^2-199/200) - 0.01;
% surf_name = 'Dumbbell_singular';
% % distance function 
% fd=@(x) x(:,1).^2 + x(:,2).^2 + 2*(x(:,3).^2).*(x(:,3).^2-199/200) - 0.04;
% grad_fd = 'too_complicated';
% 
% T = 0.068;              % final time for dumbbell

%%%%%%%%%% 
% % GenusBall
% surf_name = 'GenusBall2';
% % distance function 
% fd=@(x) (x(:,1).^2-1).^2 + (x(:,2).^2-1).^2 + (x(:,3).^2-1).^2 - 1.05;
% grad_fd = @(x) [2 .* (x(:,1).^2-1) .* 2.*x(:,1) ...
%          2 .* (x(:,2).^2-1) .* 2.*x(:,2) ...
%          2 .* (x(:,3).^2-1) .* 2.*x(:,3)];


%% loading P2 mesh
addpath('../aux_func');
[Nodes,Elements,Elements_plot] = load_mesh_p2(surf_name,fd);

dof=length(Nodes)

%% initial data
one_vec = ones(dof,1);

% % a NICE initial value on a sphere
% Nodes = 2 * Nodes;



% normal vector
Normal = load_normal_p2(surf_name,Nodes,Elements_plot,fd,grad_fd);
length_Normal = sqrt(sum(Normal.^2, 2));

% mean curvature
[M,H_init] = surface_assembly_P2_H(Nodes,Elements, Normal);


% % a NICE initial value on a sphere
% x1=Nodes(:,1); x2=Nodes(:,2); x3=Nodes(:,3);
% u_init = -6 * (1+x1) .* (1-x1) .* (1+x2) .* (1-x2) .* (1+x3) .* (1-x3);
% % u_init = u_init - ( max(u_init) - min(u_init) ) / 2;
% u_init = u_init / (2*10^1) + 0.1;
% % % % saved u^0
% % % u_init = -6 * (1+x1) .* (1-x1) .* (1+x2) .* (1-x2) .* (1+x3) .* (1-x3);
% % % % u_init = u_init - ( max(u_init) - min(u_init) ) / 2;
% % % u_init = u_init / (2*10^1) + 0;
% maxmin = [max(u_init) , min(u_init)];
% % disp('')

% initial value on elongated ellipse
r = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2 + Nodes(:,3).^2) ;
u_init = 1 * ( 10*one_vec - 30 ./ (1.25*r.^2+1) ) - 4.5;

% % initial value on cup
% % pos = Nodes(:,3) < -1;
% r = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2 + (Nodes(:,3)+0.55).^2);
% pos = r < 0.5;
% u_add = zeros(dof,1);
% u_add(pos,1) = -3;
% % u_add = -2+2*r.^0.5;
% % u_add = 10 * exp(-0.01./((r.^2+1)));
% u_init = 4*one_vec + u_add ;

% % initial value on blander cup
% % pos = Nodes(:,3) < -1;
% r = sqrt(Nodes(:,1).^2 + Nodes(:,2).^2 + Nodes(:,3).^2);
% pos = r > 0.95 & Nodes(:,3) < -0.7;
% u_add = zeros(dof,1);
% u_add(pos,1) = -0;
% % u_add = -2+2*r.^0.5;
% % u_add = 10 * exp(-0.01./((r.^2+1)));
% u_init = 1*one_vec + u_add ;

% % initial value on fat dumbbell
% pos = Nodes(:,3) > 0.25;
% u_add = zeros(dof,1);
% u_add(pos,1) = 0.8 .* ( 1-exp(-10.*Nodes(pos,3).^2) );
% u_init = 10^(-4) * one_vec + u_add;


[g_u_init,~] = func_g_dg(u_init);
w_init = [ Normal -func_F( g_u_init , H_init) ];

rmpath('../aux_func');

%% plot initial surface and initial data
figure('Position', [100, 50, 1280, 720]);
% subplot(1,3,1)
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
% % trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
% colorbar
% hold on;
% % quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% % hold off;
% axis('equal')
% title('mesh and $H_h(\cdot,0)$','interpreter','latex')

subplot(1,2,1)
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init ) %
trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
colorbar
% hold on;
% quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% hold off;
axis('equal')
% xlim([-1.5 0]);
view([1 1 -1])
title('mesh and $u_h(\cdot,0)$','interpreter','latex')

subplot(1,2,2)
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), fd(Nodes) ) %
trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), w_init(:,4) ) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), H_init) %
% trisurf(Elements_plot,Nodes(:,1),Nodes(:,2),Nodes(:,3), u_init ) %
colorbar
% colormap(hsv)
% hold on;
% quiver3(Nodes(:,1),Nodes(:,2),Nodes(:,3),w_init(:,1),w_init(:,2),w_init(:,3))
% hold off;
axis('equal')
title('mesh and $V_h(\cdot,0)$','interpreter','latex')

drawnow
disp('')


%% calling algorithm

% MCFdiff_solver(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);

% MCFdiff_plotter(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);
% MCFdiff_conservation_plotter(Nodes,Elements,Elements_plot,w_init,u_init,k,tau,m,T, flow, ['_',surf_name]);



%%%%%%%%%%%%
% plotting
%%%%%%%%%%%%
% % fig=figure('Position', [100, 50, 1280, 720]);
% % plt=0; % no plotting case, due to runscripts
% % plt=1; % only plotting
% plt=2; % video
% % plt=6; % frames
% 
% 
% limits = 1 * [-1 1; -1 1; -1 1]; % spherical
% % limits = [-1.35 1.35; -1.35 1.35; -1.75 1.75]; % fat dumbbell iMCF
% % limits = [-1 1; -1 1; -1.15 1.15]; % fat dumbbell MCFgen2
% view_angle = [1 0.82 0.42];
% % view_angle = [1 1 0];
% % view_angle = [1 0 0];
% skipping = 3;
% T0 = 0;     % starting time
% 
% % title_text = 'MCF diff';
% 
% % MCFgeneralised_plotter(Nodes,Elements,Elements_plot,k,tau,T0,T, surf_name,u_init, plt,limits,view_angle,skipping,title_text,flow,alpha);

%% mass and energy conservation

%%
% beep;

end