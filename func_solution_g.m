function u_out = func_solution_g(u0,R0,R)
% g(r) =  (1+\alpha) r^{-\alpha}
% 
% R(t) = \Big( R_0^{2 - \alpha m} - t b (2 - \alpha m) \Big)^{\frac{1}{2 - \alpha m}}
% with R(0) = R_0
% and b = m \, (1+\alpha) \Big( u_0 \, R_0^m \Big)^{-\alpha}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% dimension
m = 2;

u_out = u0 * ( R0 / R )^m ;