function MCFdiff_g_solver_no_u_test(Nodes,Elements,Elements_plot,k,n,tau,m,u0,R0,alpha,T, flow)
% 
% Convergence test for MCF diff.
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

%% figure creation
plt = 0;
% plt = 2;
% fig = figure('position',[10 50 1200 500]);

%% initial surface

% degrees of freedom
dof = length(Nodes);
% constant 1 vector
one_vec = ones(dof,1);

% saving initial mesh
Nodes0 = Nodes;
    

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initial values are exact solutions
t_new = 0;
t = [t_new];
% \Ga(x) - surface
x_new = R0 * Nodes0;
x_past(:,:,1) = x_new;
% u - diffusion variable on the surface 
u_new = u0 * one_vec;
u_past(:,1) = u_new;
% w = (\nu,V)^T - geometric variables, 
% with V = - g(u) * H , where g(u) = (1+alpha) u^(-alpha)
w_new = [func_sphere_normal(x_new) -(1+alpha)*u_new.^(-alpha) .* func_sphere_curvature(x_new,R0,m)];
w_past(:,:,1) = w_new;
M = surface_assembly_P2_mass(x_new,Elements);
Mu_past(:,1) = M * u_new;
% plotting on the fly
plotting_on_the_fly;

for ind = 1:k-1
    % current time
    t_new = ind*tau;
    t = [t t_new];
    % current radius
    Radius = func_Radius_g(t_new,R0,u0,alpha);
    % \Ga(x) - surface
    x_new = Radius * Nodes0;
    x_past(:,:,ind+1) = x_new;
    % u - diffusion variable on the surface 
    u_new = func_solution_g(u0,R0,Radius) * one_vec;
    u_past(:,ind+1) = u_new;
    % w = (\nu,V)^T - geometric variables, 
    % with V = - g(u) * H , where g(u) = (1+alpha) u^(-alpha)
    w_new = [func_sphere_normal(x_new) -(1+alpha)*u_new.^(-alpha) .* func_sphere_curvature(x_new,Radius,m)];
    w_past(:,:,ind+1) = w_new;
    % storing the vectors M*u
    [M] = surface_assembly_P2_mass(x_new,Elements);
    Mu_past(:,ind+1) = M * u_new;
    
    % plotting on the fly
    plotting_on_the_fly;
end


%% BDF coefficients
[delta,gamma] = BDF_tableau(k);

%% time Integration
for steps = k:ceil(T/tau)
    % current time
    t_new = t(end) + tau;
    
    % current radius
    Radius = func_Radius_g(t_new,R0,u0,alpha);
    
    % extrapolations
    x_tilde=zeros(dof,3);
    w_tilde=zeros(dof,4);
    for ind=1:k
        % surface
        x_tilde = x_tilde + gamma(k-ind+1) * x_past(:,:,end-k+ind);
        % dynamic variables u = (nu,V)
        w_tilde = w_tilde + gamma(k-ind+1) * w_past(:,:,end-k+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
    w_tilde(:,1:3) = w_tilde(:,1:3) ./ sqrt( sum(w_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
%     x_tilde = Radius * Nodes0;
%     u_tilde = func_solution_g(u0,R0,Radius) * one_vec;
%     w_tilde(:,1:3) = func_sphere_normal(x_tilde);
%     w_tilde(:,4) = -(1+alpha)*u_tilde.^(-alpha) .* func_sphere_curvature(x_tilde,Radius,m);
    
    %%%%%%%%%%%%%%%%
    % STEP PRE
    % contributions from the past
    dx = zeros(dof,3);
    dw = zeros(dof,4);
    for ind = 1:k
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        dw = dw + delta(ind+1) * w_past(:,:,end-ind+1);
    end
    
    %%%%%%%%%%%%%%%%
    % STEP (1)
    % the exact solution of the diffusion equation
    u_exact = func_solution_g(u0,R0,Radius) * one_vec;
    
    % assembly
    [M,A , Mxuw,Axu , f,f_nu] = surface_assembly_P2_MCFdiff_no_matu(x_tilde,Elements, w_tilde , u_exact);
    
    %%%%%%%%%%%%%%%%
    % STEP (2) - Solve the geometric evolution equations w = (nu,V)
    % (Needs: Mxwu, A, f)
    % surface FEM assembly on the EXTRAPOLATED surface
    
%     % check f itself
%     % for a sphere of radius R: |A|^2 = m / R^2
%     A2 = m / (Radius^2);
%     f_2 = M * A2 * w_tilde;
    
    % -- 1
    % the material derivative \mat_h u_h of the exact solution u_h
    [G,dG,~] = func_G_evals(u_exact);
    g_exact = G - u_exact .* dG;
    mat_u_exact = g_exact .* u_exact .* (m/Radius)^2;
    [ f_V_1 ] = surface_assembly_P2_MCFdiff_matu(x_tilde,Elements, w_tilde , u_exact, mat_u_exact);
    % -- 2
%     [G,dG,~] = func_G_evals(u_exact);
%     g_exact = G - u_exact .* dG;
%     V_exact = - g_exact .* (m/Radius);
%     K_exact = func_K(u_exact,G,dG,V_exact);
%     f_V_2 = [zeros(dof,3) , - Axu * u_exact + M * (u_exact .* V_exact .* K_exact) ];
    % -- 3
%     [G,dG,ddG] = func_G_evals(u_exact);
%     g_exact = G - u_exact .* dG;
%     dg_exact = - u_exact .* ddG;
%     f_V_3 = [zeros(dof,3) , M * ( - dg_exact .* u_exact .* (m/Radius)^3 )]; 
    % -- 4
%     [G,dG,~] = func_G_evals(u_exact);
%     g_exact = G - u_exact .* dG;
%     dg_exact = - u_exact .* ddG;
%     mat_u_exact = g_exact .* u_exact .* (m/Radius)^2;
%     f_V_4 = [zeros(dof,3) , M * ( (dg_exact ./ (g_exact.^2)) .* V_exact .* mat_u_exact )]; 
    
    f_V = f_V_1;
      
    % solving the linear system for the geometric variable w = (nu,V)
    w_new = ( delta(1)*Mxuw + tau*A ) \ ( tau * (f + f_nu + f_V ) - Mxuw * dw );
%     w_new(:,1:3) = func_sphere_normal(x_tilde);
%     w_new(:,4) = -(1+alpha)*u_new.^(-alpha) .* func_sphere_curvature(x_tilde,Radius,m);
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
%     w_new(:,1:3) = w_new(:,1:3) ./ sqrt( sum(w_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP (3) and (4) determining the velocity and integrating the surface
    % (Needs: w_new, dx)
    
    % computing new velocity
%     V_new = w_new(:,4); nu_new = u_new(:,1:3);    
%     v_new = V_new .* nu_new + rho1;
    % r.h.s. for the surface position ODE
%     rhs_x = tau * v_new - dx;
%     rhs_x = tau * ( w_new(:,4) .* w_new(:,1:3) + rho1) - dx;

    % computing new surface positions
    x_new = (1/delta(1)) * ( tau * ( w_new(:,4) .* w_new(:,1:3) ) - dx );
%     x_new = Radius * Nodes0;
    
    %%%%%%%%%%%%%%%%
    % STEP POST
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k+1)=x_new;
    x_past(:,:,1)=[];
    % new dynamic variables
    w_past(:,:,k+1)=w_new;
    w_past(:,:,1)=[];
    
    % new time
    t=[t t_new];
    
    %% plotting on the fly
    plotting_on_the_fly;
    
    %% save errors 
    % the exact solution
    Radius = func_Radius_g(t_new,R0,u0,alpha);
    x_exact = Radius * Nodes0;
    u_exact = func_solution_g(u0,R0,Radius) * one_vec;
    H_exact = func_sphere_curvature(x_exact,Radius,m);
    w_exact = [func_sphere_normal(x_exact) -(1+alpha)*u_exact.^(-alpha) .* H_exact];
    
    H_new = - w_new(:,4) ./ ((1+alpha).*u_new.^(-alpha));
    
    K = M + A;
    
    e_x = x_new - x_exact;
    e_nu = w_new(:,1:3) - w_exact(:,1:3);
    e_V = w_new(:,4) - w_exact(:,4);
    e_H = H_new - H_exact;
    e_u = zeros(dof,1);
    
    errors(1,1:steps+1) = t;
    errors(2,steps+1) = sqrt(e_x(:,1).'*K*e_x(:,1) + e_x(:,2).'*K*e_x(:,2) + e_x(:,3).'*K*e_x(:,3));
    errors(3,steps+1) = sqrt(e_nu(:,1).'*K*e_nu(:,1) + e_nu(:,2).'*K*e_nu(:,2) + e_nu(:,3).'*K*e_nu(:,3));
    errors(4,steps+1) = sqrt(e_V.'*K*e_V);
    errors(5,steps+1) = sqrt(e_H.'*K*e_H);
    errors(6,steps+1) = sqrt(e_u.'*K*e_u);
    
    % saving the errors
    save([flow,'_no_u_test/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'errors', '-ASCII');
    
%    % for plotting: the current solution
%     s1.t_new = t_new;
%     s1.x_new = x_new;
%     s1.w_new = w_new;
%     s1.u_new = u_new;
%     save(['MCFdiff_convtest/solution_at_t',num2str(t_new),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.mat'],'-struct','s1');
end

% disp('')

