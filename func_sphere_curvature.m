function H = func_sphere_curvature(Nodes,R,m)

%% mean curvature (scalar) for any sphere
H = m * (1/R) * ones(size(Nodes,1),1);