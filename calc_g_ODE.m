function calc_g_ODE
syms t;
syms R;
% syms alph;
% syms R0 R1;

R0=1;
u0=1;
m = 2;
alpha = 4.510;

b = m * (1+alpha) * ( u0 * R0^m )^(-alpha);

D = ( 2 - alpha*m );

%% R(t)
R = ( R0^D - t * b * D )^(1/D);

%% ODE
simplify( diff(R,t) - (- b * R^(alpha*m - 1) ) )

end