function MCFdiff_solver_convtest(Nodes,Elements,Elements_plot,k,n,tau,m,R0,R1,T, flow)
%
% This code numerically solves the flow coupling mean curvature flow and
% diffusion on a closed surface.
% The code generates and saves data for the convergence plots,
% generated using 'convplots_fig_(*).m'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
format long;

%% initial surface

% degrees of freedom
dof = length(Nodes);
% constant 1 vector
one_vec = ones(dof,1);

% saving initial mesh
Nodes0 = Nodes;

%% structure of variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% surface:
% --> the tensor x (dimension: dof \times 3 \times T/tau) stores 
%     the surface nodes at time t_k in its k-th layer x(:,:,k)
% --> the matrix x(:,:,k) (dimension: dof \times 3) stores the nodes of the
%     discrete surface (coordinates are column-wise, nodes row-wise)
% 
% dynamic variables:
% --> the tensor u (dimension: dof \times 4 \times T/tau) stores
%     the nodeal values of the dynamic variables (n_h,H_h) at time t_k
%     in its k-th layer u(:,:,k)
% --> the matrix u(:,:,k) (dimension: dof \times 4) stores 
%     the nodal values of n_h in its first three columns, and
%     the nodal values of H_h in its fourth column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% initial values are exact solutions
t = [0];
% \Ga(x) - surface
x_past(:,:,1) = R0 * Nodes0;
% u - diffusion variable on the surface 
u_exact = x_past(:,1,1) .* x_past(:,2,1);
u_past(:,1) = u_exact;
% w = (\nu,V)^T - geometric variables, 
% with V = - g(u) * H , where g(u) = 10 - u
w_past(:,:,1) = [func_sphere_normal(x_past(:,:,1)) -(10 - u_exact) .* func_sphere_curvature(x_past(:,:,1),R0,m)];
M = surface_assembly_P2_mass(x_past(:,:,1),Elements);
Mu_past(:,1) = M * u_exact;


for ind = 1:k-1
    % current time
    t_new = ind*tau;
    t = [t t_new];
    % current radius
    Radius = func_Radius(t_new,R0,R1);
    % \Ga(x) - surface
    x_past(:,:,ind+1) = Radius * Nodes0;
    % u - diffusion variable on the surface 
    u_exact = exp(-t_new) * x_past(:,1,ind+1) .* x_past(:,2,ind+1);
    u_past(:,ind+1) = u_exact;
    % w = (\nu,V)^T - geometric variables, 
    % with V = - g(u) * H , where g(u) = 10 - u
    w_past(:,:,ind+1) = [func_sphere_normal(x_past(:,:,ind+1)) -(10 - u_exact) .* func_sphere_curvature(x_past(:,:,ind+1),Radius,m)];
    % storing the vectors M*u
    [M] = surface_assembly_P2_mass(x_past(:,:,ind+1),Elements);
    Mu_past(:,ind+1) = M * u_exact;
end

%% BDF coefficients
[delta,gamma] = BDF_tableau(k);

%% time Integration
for steps = k:ceil(T/tau)
    % current time
    t_new = t(end) + tau;
    
    % current radius
    Radius = func_Radius(t_new,R0,R1);
    
    % extrapolations
    x_tilde=zeros(dof,3);
    w_tilde=zeros(dof,4);
    u_tilde=zeros(dof,1);
    for ind=1:k
        % surface
        x_tilde=x_tilde+gamma(k-ind+1)*x_past(:,:,end-k+ind);
        % dynamic variables u = (nu,V)
        w_tilde=w_tilde+gamma(k-ind+1)*w_past(:,:,end-k+ind);
        % diffusion variable u
        u_tilde = u_tilde + gamma(k-ind+1) * u_past(:,end-k+ind);
    end
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
%     w_tilde(:,1:3) = w_tilde(:,1:3) ./ sqrt( sum(w_tilde(:,1:3).^2,2) );
    %%%%%%%%
    
    x_tilde = Radius * Nodes0;
%     u_tilde = exp(-t_new) * x_tilde(:,1) .* x_tilde(:,2);
    w_tilde(:,1:3) = func_sphere_normal(x_tilde);
%     w_tilde(:,4) = -(10 - exp(-t_new) * x_tilde(:,1) .* x_tilde(:,2)) .* func_sphere_curvature(x_tilde,Radius,m);
    
    %%%%%%%%%%%%%%%%
    % STEP PRE
    % contributions from the past
    dx = zeros(dof,3);
    dw = zeros(dof,4);
    dMu = zeros(dof,1);
    for ind = 1:k
        dx = dx + delta(ind+1) * x_past(:,:,end-ind+1);
        dw = dw + delta(ind+1) * w_past(:,:,end-ind+1);
        dMu = dMu + delta(ind+1) * Mu_past(:,end-ind+1);
    end
    
    %%%%%%%%%%%%%%%%
    % STEP (1) - Solve the diffusion equation
    % (Needs: M, Axu)
    [M,A , Mxuw,Axu , f,f_nu] = surface_assembly_P2_MCFdiff_no_matu(x_tilde,Elements, w_tilde , u_tilde);
    
    % inhomogeneity rho_4
    rho4 = func_rho4(x_tilde,t_new,Radius,R0,R1);
    
    % solving the linear system for the diffusion variable
    u_new = ( delta(1) * M + tau * Axu ) \ ( tau * M * rho4 - dMu);
%     u_new = exp(-t_new) * x_tilde(:,1) .* x_tilde(:,2);
    Mu_new = M * u_new;
    
    % approximation of the material derivative \mat_h u_h
    % for STEP (2)
    mat_u = (1/tau) * (delta(1) * Mu_new + dMu);
    
    %%%%%%%%%%%%%%%%
    % STEP (2) - Solve the geometric evolution equations w = (nu,V)
    % (Needs: Mxwu, A, f)
    % surface FEM assembly on the EXTRAPOLATED surface
    [ f_V ] = surface_assembly_P2_MCFdiff_matu(x_tilde,Elements, w_tilde , u_tilde, mat_u);
      
    % inhomogeneity rho23 = (rho_2,rho_3)^T
    rho23 = [func_rho2(x_tilde,t_new,Radius,R0,R1) func_rho3(x_tilde,t_new,Radius,R0,R1)];
    
    % solving the linear system for the geometric variable w = (nu,V)
    w_new = ( delta(1)*Mxuw + tau*A ) \ ( tau * (f + f_nu + f_V + M * rho23) - Mxuw * dw );
%     w_new(:,1:3) = func_sphere_normal(x_tilde);
%     w_new(:,4) = -(10 - u_new) .* func_sphere_curvature(x_tilde,Radius,m);
    
    %%%%%%%%
    % projecting nu_h onto the unit sphere
%     w_new(:,1:3) = w_new(:,1:3) ./ sqrt( sum(w_new(:,1:3).^2,2) );
    %%%%%%%%
    
    %%%%%%%%%%%%%%%%
    % STEP (3) and (4) determining the velocity and integrating the surface
    % (Needs: w_new, dx)
    
    % inhomogeneity rho_1
    rho1 = func_rho1(x_tilde,t_new,Radius,R0,R1);
    
    % computing new velocity
%     V_new = w_new(:,4); nu_new = u_new(:,1:3);    
%     v_new = V_new .* nu_new + rho1;
    % r.h.s. for the surface position ODE
%     rhs_x = tau * v_new - dx;
%     rhs_x = tau * ( w_new(:,4) .* w_new(:,1:3) + rho1) - dx;

    % computing new surface positions
    x_new = (1/delta(1)) * ( tau * ( w_new(:,4) .* w_new(:,1:3) + rho1 ) - dx );
%     x_new = Radius * Nodes0;
    
    %%%%%%%%%%%%%%%%
    % STEP POST
    % updating the solutions and the time vector
    % new surface
    x_past(:,:,k+1)=x_new;
    x_past(:,:,1)=[];
    % new dynamic variables
    w_past(:,:,k+1)=w_new;
    w_past(:,:,1)=[];
    % new diffusion variable
    u_past(:,k+1)=u_new;
    u_past(:,1)=[];
    Mu_past(:,k+1) = Mu_new;
    Mu_past(:,1)=[];
    
    % new time
    t=[t t_new];
    
       
    %% save errors 
    % the exact solution
    x_exact = Radius*Nodes0;
    u_exact = exp(-t_new) .* x_exact(:,1) .* x_exact(:,2);
    w_exact = [func_sphere_normal(x_exact) -(10 - u_exact) .* func_sphere_curvature(x_exact,Radius,m)];
    H_exact = func_sphere_curvature(x_exact,Radius,m);
    
    H_new = - w_new(:,4) ./ (10 - u_new);
    
    K = M + A;
    
    e_x = x_new - x_exact;
    e_nu = w_new(:,1:3) - w_exact(:,1:3);
    e_V = w_new(:,4) - w_exact(:,4);
    e_H = H_new - H_exact;
    e_u = u_new - u_exact;
    
    errors(1,1:steps+1) = t;
    errors(2,steps+1) = sqrt(e_x(:,1).'*K*e_x(:,1) + e_x(:,2).'*K*e_x(:,2) + e_x(:,3).'*K*e_x(:,3));
    errors(3,steps+1) = sqrt(e_nu(:,1).'*K*e_nu(:,1) + e_nu(:,2).'*K*e_nu(:,2) + e_nu(:,3).'*K*e_nu(:,3));
    errors(4,steps+1) = sqrt(e_V.'*K*e_V);
    errors(5,steps+1) = sqrt(e_H.'*K*e_H);
    errors(6,steps+1) = sqrt(e_u.'*K*e_u);
    
    % saving the errors
    save([flow,'_convtest/errors_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.txt'], 'errors', '-ASCII');
    
%    % for plotting: the current solution
%     s1.t_new = t_new;
%     s1.x_new = x_new;
%     s1.w_new = w_new;
%     s1.u_new = u_new;
%     save(['MCFdiff_convtest/solution_at_t',num2str(t_new),'_BDF',num2str(k),'_n',num2str(n),'_tau',num2str(tau),'.mat'],'-struct','s1');
end

% disp('')

