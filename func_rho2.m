function rho2_out = func_rho2(Nodes,t,Radius,R0,R1)
% inhomogeneity for \nu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
% coordinates
x = Nodes(:,1);
y = Nodes(:,2);
z = Nodes(:,3);

% exact solution
% u = x.*y*exp(-t);

%% \diff R(t) = dtR
% dtR=(1-Radius/R1)*Radius;
rho2_out = [(2.*y.*(Radius.^2 - 2.*x.^2))./(Radius.^3.*(10.*exp(t) - x.*y)) , ...
            (2.*x.*(Radius.^2 - 2.*y.^2))./(Radius.^3.*(10.*exp(t) - x.*y)) , ...
            -(4.*x.*y.*z)./(Radius.^3.*(10.*exp(t) - x.*y))];

end